<?php

class Main extends Controller {
    /*
     * Home page
     * Presents campaign information and a random Contact
     */
    function home($f3,$args) {
        // Select a random contact
        //TODO: use weight for random
        $rand_id = rand(0, count($f3->get('contacts'))-1);
        $contacts = $f3->get('contacts');
        $f3->set('contact', $contacts[$rand_id]);
        $f3->set('random', rand(0, 2));
        $f3->set('block_content','home.html');
    }

    /*
     * Feedback page
     * Form for a feedback after a call
     * GET: show the form
     * POST: send the form to the campaign API and show thank you
     */
    function feedbackform($f3, $args) {
        //GET
        if ($f3->get('VERB') == 'GET'){
            $categories = Api::get_feedback_categories();
            $f3->set('feedback_categories', $categories['categories']);

            $contact_id = $f3->get('POST.contact_id');
            $f3->set("contact_id", $contact_id);

            //TODO: create call
            //TODO: start call

            $f3->set('block_content', 'feedbackform.html');
        }
        //POST
        elseif ($f3->get('VERB') == 'POST'){
            $contact_id = $f3->get('POST.contact_id');
            $feedback = $f3->get('POST.feedback');
            $category = $f3->get('POST.feedback_category');

            //send feedback to campaign
            $f3->set('post_feedback_result', Api::post_feedback($contact_id, $feedback, $category));
            $f3->set('block_content', 'thankyou.html');
        }
    }

    /*
     * Feedback SIP function
     */
    function feedbacksip($f3, $args) {

    }

    /*
     * call Page
     * Form to call
     * GET: show the form
     * POST: send the form and GET feedbackform
     */
    function call($f3, $args) {
        //GET
        if ($f3->get('VERB') == 'GET') {
            $f3->set('contact', Api::get_contact()); //$args['id']

            $f3->set('block_content', 'call.html');
        }
        //POST
        elseif ($f3->get('VERB') == 'POST'){
            //GET feedbackform
            $f3->set('VERB', 'GET');
            $this->feedbackform($f3, $args);
        }
    }

    function contactslist($f3, $args) {
        $contacts = Api::get_contacts();
        $f3->set('contacts', $contacts);
        $f3->set('random', 2);

        $f3->set('block_content', 'contactslist.html');
    }

    function argumentation($f3, $args) {
        $arguments = Api::get_arguments();
        $f3->set('arguments', $arguments);

        $f3->set('block_content', 'argumentation.html');
    }

};
