<?php

/*
 * Static class for REST API communication
 */
class Api {
    // Generic get function for REST API
    static function get($url) {
        $curl = curl_init();
        $url = API_BASE . $url . "/?format=json";
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    // Generic post function for REST API
    static function post($url, $data) {
        $curl = curl_init();
        $url = API_BASE . $url;
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    // Asking for campaign informations
    static function get_campaign() {
        $json = Api::get("campaigns/" . CAMPAIGN_ID);
        return json_decode($json, true);
    }

    // Asking for complete list of contacts
    static function get_contacts() {
        $json = Api::get("campaigns/" . CAMPAIGN_ID . "/contacts");
        return json_decode($json, true);
    }

    // Asking for complete list of groups
    static function get_groups() {
        $json = Api::get("campaigns/" . CAMPAIGN_ID . "/groups");
        return json_decode($json, true);
    }

    // Asking for complete list of feedback categories
    static function get_feedback_categories() {
        $json = Api::get("campaigns/" . CAMPAIGN_ID . "/feedback/categories");
        return json_decode($json, true);
    }

    // Asking for arguments by language
    static function get_arguments($language='en') {
        $json = Api::get("campaigns/" . CAMPAIGN_ID . "/arguments");
        return json_decode($json, true);
    }

    // Posting a feedback
    static function post_feedback($contact_id, $comment, $feedback_category='') {
        $data = array(
            "contact_id"=>$contact_id,
            "comment"=>$comment,
            "feedback_category"=>$feedback_category
        );
        $json = Api::post("campaigns/" . CAMPAIGN_ID . "/feedback/add", $data);
        return json_decode($json, true);
    }

};

