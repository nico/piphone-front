<?php
/*
 * Fake REST API from Campaign module
 *
 * Uses datas folder to retrieve informations
 */

class RestApi {
    function return_datas($filename) {
        $f = fopen($filename, 'r');
        $result = fread($f, filesize($filename));
        fclose($f);
        return $result;
    }

    function campaign($f3,$args) {
        echo $this->return_datas('app/datas/campaign.json');
    }

    function contacts($f3,$args) {
        echo $this->return_datas('app/datas/contacts.json');
    }

    function categories($f3,$args) {
        // List of feedback categories
        echo'{"categories": [
                "category1",
                "category2",
                "category3"
                ]
            }';
    }

    function feedback_add($f3,$args) {
        // List of contacts
    }

    function arguments($f3,$args) {
        // List of arguments
    }

};

