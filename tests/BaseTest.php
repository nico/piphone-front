<?php
/*
 * Base class for testing
 *
 * Sub classes could set their own "set up" / "tear down" methods but should
 * always call the parents one by using parent::setup() / parent::teardown()
 *
 */

// Framework instance
require_once('../lib/base.php');

class BaseTest {
    protected $test;
    protected $f3;

    function __construct() {
		$this->f3 = Base::instance();
		$this->f3->config('config.ini');
		$this->f3->config('../app/routes.ini');
    }

    function setup() {
        $this->test = new Test;
    }

    function teardown() {
        // Nothing yet
    }

    function run($output="text") {
        // Set Up
        $this->setup();
        // Adding all tests
        foreach (get_class_methods($this) as $method) {
            if (substr($method, 0, 4)=='test') {
                try {
                    $this->$method();
                } catch (Exception $e) {
                    //echo $e;
                }
            }
        }
        // Tear Down
        $this->teardown();
        // Getting tests results
        $this->results = $this->test->results();

        // Show results
        switch ($output) {
            default:
                $this->output_text();
        }
    }

    function sanitize_output($string) {
        return str_replace('_', ' ', substr($string,5));
    }

    // Outputs results in raw text
    function output_text() {
        $failed = array();
        echo "Testing " . $this->sanitize_output(get_class($this)) . "\n";
        echo "================================================================\n";
        echo "Launching " . count($this->results) . " tests:\n";
        foreach ($this->results as $result) {
            if ($result['status']) {
                echo "P";
            } else {
                echo "F";
                $failed[] = $result;
            }
        }
        echo "\n----------------------------------------------------------------\n";
        echo count($failed) . " on " . count($this->results) . " test(s) failed\n";
        foreach ($failed as $result) {
            echo "On " . $result['source'] . "\n";
            echo "    " . $result['text'] . "\n";
        }
    }
}
